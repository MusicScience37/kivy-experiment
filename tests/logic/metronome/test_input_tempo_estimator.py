"""InputTempoEstimator のテスト。"""

import pytest

from kivy_experiment.logic.metronome.input_tempo_estimator import InputTempoEstimator


def test_calculate_tempo_from_two_time() -> None:
    """2 つの時刻からテンポを計算する。"""
    tempo_calculator = InputTempoEstimator()
    time1 = 0.123
    time2 = time1 + 2.0 / 3.0

    tempo_calculator.update(time1)
    tempo_calculator.update(time2)

    assert tempo_calculator.current_tempo == pytest.approx(90.0)
