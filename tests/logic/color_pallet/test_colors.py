"""色の各種表現のテスト。"""

from kivy_experiment.logic.color_pallet.colors import (
    HSL,
    HSV,
    RGB,
    hsl2rgb,
    hsv2rgb,
    rgb2hsl,
    rgb2hsv,
)


class TestRGB:
    """RGB クラスのテスト。"""

    def test_convert_to_color_code1(self) -> None:
        """カラーコードへ変換する。(1)"""
        rgb = RGB(0, 0, 0)

        color_code = rgb.as_color_code()

        assert color_code == "#000000"

    def test_convert_to_color_code2(self) -> None:
        """カラーコードへ変換する。(2)"""
        rgb = RGB(0xFF, 0x0A, 0x12)

        color_code = rgb.as_color_code()

        assert color_code == "#FF0A12"


class TestRGB2HSV:
    """rgb2hsv 関数のテスト。"""

    def test_convert_rgb_to_hsv1(self) -> None:
        """RGB から HSV へ変換する。(1)"""
        rgb = RGB(0, 0, 0)

        hsv = rgb2hsv(rgb)

        assert hsv == HSV(0, 0, 0)

    def test_convert_rgb_to_hsv2(self) -> None:
        """RGB から HSV へ変換する。(2)"""
        rgb = RGB(255, 255, 255)

        hsv = rgb2hsv(rgb)

        assert hsv == HSV(0, 0, 100)

    def test_convert_rgb_to_hsv3(self) -> None:
        """RGB から HSV へ変換する。(3)"""
        rgb = RGB(85, 139, 45)

        hsv = rgb2hsv(rgb)

        assert hsv == HSV(94, 67, 54)


class TestHSV2RGB:
    """hsv2rgb 関数のテスト。"""

    def test_convert_hsv_to_rgb1(self) -> None:
        """HSV から RGB へ変換する。(1)"""
        hsv = HSV(0, 0, 0)

        rgb = hsv2rgb(hsv)

        assert rgb == RGB(0, 0, 0)

    def test_convert_hsv_to_rgb2(self) -> None:
        """HSV から RGB へ変換する。(2)"""
        hsv = HSV(0, 0, 100)

        rgb = hsv2rgb(hsv)

        assert rgb == RGB(255, 255, 255)

    def test_convert_hsv_to_rgb3(self) -> None:
        """HSV から RGB へ変換する。(3)"""
        hsv = HSV(158, 100, 100)

        rgb = hsv2rgb(hsv)

        assert rgb == RGB(0, 255, 161)

    def test_convert_hsv_to_rgb4(self) -> None:
        """HSV から RGB へ変換する。(4)"""
        hsv = HSV(360, 0, 100)

        rgb = hsv2rgb(hsv)

        assert rgb == RGB(255, 255, 255)

    def test_convert_hsv_to_rgb5(self) -> None:
        """HSV から RGB へ変換する。(5)"""
        hsv = HSV(241, 41, 72)

        rgb = hsv2rgb(hsv)

        assert rgb == RGB(109, 108, 183)


class TestRGB2HSL:
    """rgb2hsl 関数のテスト。"""

    def test_convert_rgb_to_hsl1(self) -> None:
        """RGB から HSL へ変換する。(1)"""
        rgb = RGB(0, 0, 0)

        hsl = rgb2hsl(rgb)

        assert hsl == HSL(0, 0, 0)

    def test_convert_rgb_to_hsl2(self) -> None:
        """RGB から HSL へ変換する。(2)"""
        rgb = RGB(255, 255, 255)

        hsl = rgb2hsl(rgb)

        assert hsl == HSL(0, 0, 100)

    def test_convert_rgb_to_hsl3(self) -> None:
        """RGB から HSL へ変換する。(2)"""
        rgb = RGB(74, 231, 59)

        hsl = rgb2hsl(rgb)

        assert hsl == HSL(114, 78, 56)


class TestHSL2RGB:
    """hsl2rgb 関数のテスト。"""

    def test_convert_hsl_to_rgb1(self) -> None:
        """HSL から RGB へ変換する。(1)"""
        hsl = HSL(0, 0, 0)

        rgb = hsl2rgb(hsl)

        assert rgb == RGB(0, 0, 0)

    def test_convert_hsl_to_rgb2(self) -> None:
        """HSL から RGB へ変換する。(2)"""
        hsl = HSL(0, 0, 100)

        rgb = hsl2rgb(hsl)

        assert rgb == RGB(255, 255, 255)

    def test_convert_hsl_to_rgb3(self) -> None:
        """HSL から RGB へ変換する。(3)"""
        hsl = HSL(158, 100, 60)

        rgb = hsl2rgb(hsl)

        assert rgb == RGB(51, 255, 180)

    def test_convert_hsl_to_rgb4(self) -> None:
        """HSL から RGB へ変換する。(4)"""
        hsl = HSL(360, 100, 50)

        rgb = hsl2rgb(hsl)

        assert rgb == RGB(255, 0, 0)

    def test_convert_hsl_to_rgb5(self) -> None:
        """HSL から RGB へ変換する。(5)"""
        hsl = HSL(241, 41, 72)

        rgb = hsl2rgb(hsl)

        assert rgb == RGB(155, 154, 212)
