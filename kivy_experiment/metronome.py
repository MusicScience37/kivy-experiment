"""メトロノームのアプリ。"""

import time

import kivy.app
import kivy.properties
import kivy.uix.boxlayout

from kivy_experiment.logic.metronome.input_tempo_estimator import InputTempoEstimator


class MetronomeWidget(kivy.uix.boxlayout.BoxLayout):
    """メトロノームのアプリのウィジェット。"""

    tempo_value_label_widget = kivy.properties.ObjectProperty()

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self._tempo_calculator = InputTempoEstimator()

    def on_tap_button(self) -> None:
        """Tap ボタンが押された場合の処理。"""
        self._tempo_calculator.update(time.monotonic())
        tempo_value = self._tempo_calculator.current_tempo
        if tempo_value is not None:
            tempo_str = str(int(tempo_value))
            self.tempo_value_label_widget.text = tempo_str


class MetronomeApp(kivy.app.App):
    """テンポを推定するアプリ。"""

    def build(self) -> MetronomeWidget:
        """ウィジェットを構築する。

        Returns:
            MetronomeWidget: ウィジェット。
        """
        return MetronomeWidget()


if __name__ == "__main__":
    MetronomeApp().run()
