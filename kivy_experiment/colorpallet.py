"""カラーパレットのアプリ。"""

import enum
import logging
import typing

import kivy.app
import kivy.core.clipboard
import kivy.uix.boxlayout
import kivy.uix.label
import kivy.uix.textinput
import kivy.uix.widget

from kivy_experiment.logic.color_pallet.color_selection import ColorSelection

LOGGER = logging.getLogger(__name__)


class InputWidgetType(enum.Enum):
    """色の入力に使用されたウィジェットの種類。"""

    COLOR_CODE_INPUT = enum.auto()
    RGB_INPUT = enum.auto()
    HSV_INPUT = enum.auto()
    HSL_INPUT = enum.auto()
    LCH_INPUT = enum.auto()


class BackgroundColorWidget(kivy.uix.widget.Widget):
    """背景色の設定をするウィジェット。"""


class CurrentColorWidget(kivy.uix.label.Label, BackgroundColorWidget):
    """現在の色を表示するウィジェット。"""

    def update_color(self, selection: ColorSelection) -> None:
        """色を更新する。

        Args:
            rgb (RGB): RGB による色表現。
        """
        rgb = selection.rgb
        # pylint: disable=W0201
        # kv ファイルで定義するプロパティは __init__ で定義するとかえって動作しなくなる。
        self.background_color = [rgb.r / 255.0, rgb.g / 255.0, rgb.b / 255.0]

        self.color_code = selection.color_code
        lightness = selection.hsl.l
        if lightness <= 50:
            self.color = [1.0, 1.0, 1.0]
        else:
            self.color = [0.0, 0.0, 0.0]

    def copy_current_color_code(self) -> None:
        """現在の色コードをクリップボードにコピーする。"""
        LOGGER.debug(
            "CurrentColorWidget: Copy current color code (%s).", self.color_code
        )
        kivy.core.clipboard.Clipboard.copy(self.color_code)


class ThreeValuesInputWidget(kivy.uix.boxlayout.BoxLayout):
    """3つの値を入力するウィジェット。"""

    def update_text(self, text1: str | int, text2: str | int, text3: str | int) -> None:
        """テキストを更新する。

        Args:
            text1 (str | int): 1 つ目の値のテキスト。
            text2 (str | int): 2 つ目の値のテキスト。
            text3 (str | int): 3 つ目の値のテキスト。
        """
        self.ids.text1_input.text = str(text1)
        self.ids.text2_input.text = str(text2)
        self.ids.text3_input.text = str(text3)

    def bind_text_callback(
        self, callback: typing.Callable[[str, str, str], None]
    ) -> None:
        """テキストの変更に対するコールバック関数を指定する。

        Args:
            callback (typing.Callable[[str, str, str], None]): コールバック関数。
        """

        def wrapped_callback(instance: kivy.uix.textinput.TextInput, _):
            if not instance.focus:
                return

            text1 = self.ids.text1_input.text
            text2 = self.ids.text2_input.text
            text3 = self.ids.text3_input.text
            callback(text1, text2, text3)

        self.ids.text1_input.bind(text=wrapped_callback)
        self.ids.text2_input.bind(text=wrapped_callback)
        self.ids.text3_input.bind(text=wrapped_callback)


class ColorPalletWidget(kivy.uix.boxlayout.BoxLayout):
    """カラーパレットのウィジェット。"""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._selection = ColorSelection()

        def color_code_text_callback(instance: kivy.uix.textinput.TextInput, _):
            if not instance.focus:
                return
            self.on_color_code_changed(instance.text)

        self.ids.color_code_input.bind(text=color_code_text_callback)
        self.ids.rgb_input.bind_text_callback(self.on_rgb_changed)
        self.ids.hsv_input.bind_text_callback(self.on_hsv_changed)
        self.ids.hsl_input.bind_text_callback(self.on_hsl_changed)
        self.ids.lch_input.bind_text_callback(self.on_lch_changed)

    def on_color_code_changed(self, color_code: str) -> None:
        """カラーコードの値が更新された時の処理。

        Args:
            color_code (str): カラーコード。
        """
        if self._selection.change_color_code(color_code):
            self._update_colors(InputWidgetType.COLOR_CODE_INPUT)

    def on_rgb_changed(self, r_str: str, g_str: str, b_str: str) -> None:
        """RGB の値が更新された時の処理。

        Args:
            r_str (str): R の値。
            g_str (str): G の値。
            b_str (str): B の値。
        """
        if self._selection.change_rgb_from_text(r_str, g_str, b_str):
            self._update_colors(InputWidgetType.RGB_INPUT)

    def on_hsv_changed(self, h_str: str, s_str: str, v_str: str) -> None:
        """HSV の値が更新されたときの処理。

        Args:
            h_str (str): H の値。
            s_str (str): S の値。
            v_str (str): V の値。
        """
        if self._selection.change_hsv_from_text(h_str, s_str, v_str):
            self._update_colors(InputWidgetType.HSV_INPUT)

    def on_hsl_changed(self, h_str: str, s_str: str, l_str: str) -> None:
        """HSL の値が更新されたときの処理。

        Args:
            h_str (str): H の値。
            s_str (str): S の値。
            l_str (str): L の値。
        """
        if self._selection.change_hsl_from_text(h_str, s_str, l_str):
            self._update_colors(InputWidgetType.HSL_INPUT)

    def on_lch_changed(self, l_str: str, c_str: str, h_str: str) -> None:
        """LCH の値が更新されたときの処理。

        Args:
            l_str (str): L の値。
            c_str (str): C の値。
            h_str (str): h の値。
        """
        if self._selection.change_lch_from_text(l_str, c_str, h_str):
            self._update_colors(InputWidgetType.LCH_INPUT)

    def _update_colors(self, input_widget_type: InputWidgetType) -> None:
        """色の表示を更新する。

        Args:
            input_widget_type (InputWidgetType): 入力に使用されたウィジェットの種類。
        """
        # 現在の色の表示を更新。
        # Kivy は 0 から 1 の浮動小数点数で色を扱う。
        self.ids.current_color.update_color(self._selection)

        if input_widget_type != InputWidgetType.COLOR_CODE_INPUT:
            self.ids.color_code_input.text = self._selection.color_code

        if input_widget_type != InputWidgetType.RGB_INPUT:
            rgb = self._selection.rgb
            self.ids.rgb_input.update_text(rgb.r, rgb.g, rgb.b)

        if input_widget_type != InputWidgetType.HSV_INPUT:
            hsv = self._selection.hsv
            self.ids.hsv_input.update_text(hsv.h, hsv.s, hsv.v)

        if input_widget_type != InputWidgetType.HSL_INPUT:
            hsl = self._selection.hsl
            self.ids.hsl_input.update_text(hsl.h, hsl.s, hsl.l)

        if input_widget_type != InputWidgetType.LCH_INPUT:
            lch = self._selection.lch
            self.ids.lch_input.update_text(lch.l, lch.c, lch.h)


class ColorPalletApp(kivy.app.App):
    """カラーパレットのアプリ。"""

    def build(self) -> ColorPalletWidget:
        """ウィジェットを構築する。

        Returns:
            ColorPalletWidget: ウィジェット。
        """
        return ColorPalletWidget()


if __name__ == "__main__":
    ColorPalletApp().run()
