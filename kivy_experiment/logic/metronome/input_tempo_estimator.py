"""入力されたテンポを推定するクラス。"""

import logging
import typing

LOGGER = logging.getLogger(__name__)


class InputTempoEstimator:
    """入力されたテンポを推定するクラス。"""

    def __init__(self) -> None:
        """コンストラクタ。"""
        self._previous_time: typing.Optional[float] = None
        self._current_tempo: typing.Optional[float] = None

    def update(self, current_time: float) -> None:
        """テンポを更新する。

        Args:
            current_time (float): 現在時刻を time.monotonic() 関数で測定した値。
        """
        if self._previous_time is not None:
            time_difference = current_time - self._previous_time
            self._current_tempo = 60.0 / time_difference
            LOGGER.debug(
                "TempoCalculator: time: %s, tempo: %s",
                time_difference,
                self._current_tempo,
            )

        self._previous_time = current_time

    @property
    def current_tempo(self) -> typing.Optional[float]:
        """現在のテンポを取得する。

        Returns:
            typing.Optional[float]: 現在のテンポ。
        """
        return self._current_tempo
