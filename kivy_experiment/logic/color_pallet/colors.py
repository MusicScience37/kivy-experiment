"""色の各種表現の実装。"""

import dataclasses
import math

import cv2 as cv
import numpy


@dataclasses.dataclass
class RGB:
    """RGB 色空間による表現。

    Attributes:
        r (int): 赤成分。（0 ～ 255）
        g (int): 緑成分。（0 ～ 255）
        b (int): 青成分。（0 ～ 255）
    """

    r: int
    g: int
    b: int

    def as_color_code(self) -> str:
        """カラーコードに変換する。

        Returns:
            str: カラーコード。
        """
        return f"#{self.r:02X}{self.g:02X}{self.b:02X}"


@dataclasses.dataclass
class HSV:
    """HSV 色空間による表現。

    Attributes:
        h (int): Hue. (0 ～ 360)
        s (int): Saturation. (0 ～ 100)
        v (int): Value. (0 ～ 100)
    """

    h: int
    s: int
    v: int


@dataclasses.dataclass
class HSL:
    """HSL 色空間による表現。

    Attributes:
        h (int): Hue. (0 ～ 360)
        s (int): Saturation. (0 ～ 100)
        l (int): Lightness. (0 ～ 100)
    """

    h: int
    s: int
    l: int


@dataclasses.dataclass
class Lch:
    """LCh 色空間による表現。

    Attributes:
        l (float): Lightness. (0 ～ 100)
        c (float): Chroma. (0 ～ 100)
        h (float): Hue. (0 ～ 360)
    """

    l: float
    c: float
    h: float


def rgb2hsv(rgb: RGB) -> HSV:
    """RGB を HSV に変換する。

    Args:
        rgb (RGB): RGB 色空間による表現。

    Returns:
        HSV: HSV 色空間による表現。
    """
    rgb_mat = numpy.array(
        [[[rgb.r / 255.0, rgb.g / 255.0, rgb.b / 255.0]]], dtype=numpy.float32
    )
    hsv_mat = cv.cvtColor(rgb_mat, cv.COLOR_RGB2HSV)
    return HSV(
        h=int(hsv_mat[0][0][0]),
        s=int(hsv_mat[0][0][1] * 100.0),
        v=int(hsv_mat[0][0][2] * 100.0),
    )


def hsv2rgb(hsv: HSV) -> RGB:
    """HSV を RGB に変換する。

    Args:
        hsv (HSV): HSV 色空間による表現。

    Returns:
        RGB: RGB 色空間による表現。
    """
    hsv_mat = numpy.array(
        [[[float(hsv.h), hsv.s / 100.0, hsv.v / 100.0]]], dtype=numpy.float32
    )
    rgb_mat = cv.cvtColor(hsv_mat, cv.COLOR_HSV2RGB)
    return RGB(
        r=int(rgb_mat[0][0][0] * 255.0),
        g=int(rgb_mat[0][0][1] * 255.0),
        b=int(rgb_mat[0][0][2] * 255.0),
    )


def rgb2hsl(rgb: RGB) -> HSL:
    """RGB を HSL に変換する。

    Args:
        rgb (RGB): RGB 色空間による表現。

    Returns:
        HSL: HSL 色空間による表現。
    """
    rgb_mat = numpy.array(
        [[[rgb.r / 255.0, rgb.g / 255.0, rgb.b / 255.0]]], dtype=numpy.float32
    )
    # OpenCV 内では HSL でなく HLS になっている。
    hls_mat = cv.cvtColor(rgb_mat, cv.COLOR_RGB2HLS)
    return HSL(
        h=int(hls_mat[0][0][0]),
        s=int(hls_mat[0][0][2] * 100.0),
        l=int(hls_mat[0][0][1] * 100.0),
    )


def hsl2rgb(hsl: HSL) -> RGB:
    """HSL を RGB に変換する。

    Args:
        hsl (HSL): HSL 色空間による表現。

    Returns:
        RGB: RGB 色空間による表現。
    """
    hls_mat = numpy.array(
        [[[float(hsl.h), hsl.l / 100.0, hsl.s / 100.0]]], dtype=numpy.float32
    )
    rgb_mat = cv.cvtColor(hls_mat, cv.COLOR_HLS2RGB)
    return RGB(
        r=int(rgb_mat[0][0][0] * 255.0),
        g=int(rgb_mat[0][0][1] * 255.0),
        b=int(rgb_mat[0][0][2] * 255.0),
    )


def rgb2lch(rgb: RGB) -> Lch:
    """RGB を LCh に変換する。

    Args:
        rgb (RGB): RGB 色空間による表現。

    Returns:
        Lch: LCh 色空間による表現。
    """
    rgb_mat = numpy.array(
        [[[rgb.r / 255.0, rgb.g / 255.0, rgb.b / 255.0]]], dtype=numpy.float32
    )
    lab_mat = cv.cvtColor(rgb_mat, cv.COLOR_RGB2Lab)
    l = lab_mat[0][0][0]
    a = lab_mat[0][0][1]
    b = lab_mat[0][0][2]
    c = math.sqrt(a * a + b * b)
    h = math.atan2(b, a) * 180.0 / math.pi
    return Lch(l=l, c=c, h=h)


def lch2rgb(lch: Lch) -> RGB:
    """LCh を RGB に変換する。

    Args:
        lch (Lch): LCh 色空間による表現。

    Returns:
        RGB: RGB 色空間による表現。
    """
    h_rad = lch.h / 180.0 * math.pi
    a = lch.c * math.cos(h_rad)
    b = lch.c * math.sin(h_rad)
    lab_mat = numpy.array([[[lch.l, a, b]]], dtype=numpy.float32)
    rgb_mat = cv.cvtColor(lab_mat, cv.COLOR_Lab2RGB)
    return RGB(
        r=int(rgb_mat[0][0][0] * 255.0),
        g=int(rgb_mat[0][0][1] * 255.0),
        b=int(rgb_mat[0][0][2] * 255.0),
    )
