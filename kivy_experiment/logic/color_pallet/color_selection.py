"""色の選択の処理を実装するクラス。"""

import logging
import re

from kivy_experiment.logic.color_pallet.colors import (
    HSL,
    HSV,
    RGB,
    Lch,
    hsl2rgb,
    hsv2rgb,
    lch2rgb,
    rgb2hsl,
    rgb2hsv,
    rgb2lch,
)

LOGGER = logging.getLogger(__name__)


class ColorSelection:
    """色の選択の処理を実装するクラス。"""

    def __init__(self):
        self._color_code = "#000000"
        self._color_code_regex = re.compile(
            r"#([0-9A-F][0-9A-F])([0-9A-F][0-9A-F])([0-9A-F][0-9A-F])"
        )
        self._rgb = RGB(0, 0, 0)
        self._hsv = HSV(0, 0, 0)
        self._hsl = HSL(0, 0, 0)
        self._lch = rgb2lch(self._rgb)

    def change_color_code(self, color_code: str) -> bool:
        """カラーコードを変更する。

        Args:
            color_code (str): カラーコード。

        Returns:
            bool: 変更に成功したかどうか。
        """
        LOGGER.debug("ColorSelection: Color code changed.")

        match = self._color_code_regex.match(color_code)
        if not match:
            LOGGER.debug("ColorSelection: Invalid color code: %s", color_code)
            return False

        r = int(match.group(1), base=16)
        g = int(match.group(2), base=16)
        b = int(match.group(3), base=16)
        self._color_code = color_code
        self._rgb = RGB(r, g, b)
        self._hsv = rgb2hsv(self._rgb)
        self._hsl = rgb2hsl(self._rgb)
        self._lch = rgb2lch(self._rgb)
        return True

    def change_rgb_from_text(self, r_str: str, g_str: str, b_str: str) -> bool:
        """RGB の値を変更する。

        Args:
            r_str (str): R の値。
            g_str (str): G の値。
            b_str (str): B の値。

        Returns:
            bool: 変更に成功したかどうか。
        """
        LOGGER.debug("ColorSelection: RGB changed.")

        try:
            r = int(r_str)
            g = int(g_str)
            b = int(b_str)
            if not (0 <= r <= 255 and 0 <= g <= 255 and 0 <= b <= 255):
                raise ValueError("Invalid RGB value.")
        except ValueError:
            LOGGER.debug("ColorSelection: Invalid RGB value.", exc_info=True)
            return False

        self._rgb = RGB(r, g, b)
        self._color_code = self._rgb.as_color_code()
        self._hsv = rgb2hsv(self._rgb)
        self._hsl = rgb2hsl(self._rgb)
        self._lch = rgb2lch(self._rgb)
        return True

    def change_hsv_from_text(self, h_str: str, s_str: str, v_str: str) -> bool:
        """HSV の値を変更する。

        Args:
            h_str (str): Hue の値。
            s_str (str): Saturation の値。
            v_str (str): Value の値。

        Returns:
            bool: 変更に成功したかどうか。
        """
        LOGGER.debug("ColorSelection: HSV changed.")

        try:
            h = int(h_str)
            s = int(s_str)
            v = int(v_str)
            if not (0 <= h <= 360 and 0 <= s <= 100 and 0 <= v <= 100):
                raise ValueError("Invalid HSV value.")
        except ValueError:
            LOGGER.debug("ColorSelection: Invalid HSV value.", exc_info=True)
            return False

        self._hsv = HSV(h, s, v)
        self._rgb = hsv2rgb(self._hsv)
        self._color_code = self._rgb.as_color_code()
        self._hsl = rgb2hsl(self._rgb)
        self._lch = rgb2lch(self._rgb)
        return True

    def change_hsl_from_text(self, h_str: str, s_str: str, l_str: str) -> bool:
        """HSL の値を変更する。

        Args:
            h_str (str): Hue の値。
            s_str (str): Saturation の値。
            l_str (str): Lightness の値。

        Returns:
            bool: 変更に成功したかどうか。
        """
        LOGGER.debug("ColorSelection: HSL changed.")

        try:
            h = int(h_str)
            s = int(s_str)
            l = int(l_str)
            if not (0 <= h <= 360 and 0 <= s <= 100 and 0 <= l <= 100):
                raise ValueError("Invalid HSL value.")
        except ValueError:
            LOGGER.debug("ColorSelection: Invalid HSL value.", exc_info=True)
            return False

        self._hsl = HSL(h, s, l)
        self._rgb = hsl2rgb(self._hsl)
        self._color_code = self._rgb.as_color_code()
        self._hsv = rgb2hsv(self._rgb)
        self._lch = rgb2lch(self._rgb)
        return True

    def change_lch_from_text(self, l_str: str, c_str: str, h_str: str) -> bool:
        """LCh の値を変更する。

        Args:
            l_str (str): Lightness の値。
            c_str (str): Chroma の値。
            h_str (str): Hue の値。

        Returns:
            bool: 変更に成功したかどうか。
        """
        LOGGER.debug("ColorSelection: LCh changed.")

        try:
            l = float(l_str)
            c = float(c_str)
            h = float(h_str)
            if not (0.0 <= l <= 100.0 and 0.0 <= c <= 182.0 and 0.0 <= h <= 360.0):
                raise ValueError("Invalid LCh value.")
        except ValueError:
            LOGGER.debug("ColorSelection: Invalid LCh value.", exc_info=True)
            return False

        new_lch = Lch(l=l, c=c, h=h)
        try:
            self._rgb = lch2rgb(new_lch)
        except RuntimeError:
            LOGGER.debug("ColorSelection: Invalid LCh value.", exc_info=True)
            return False

        self._color_code = self._rgb.as_color_code()
        self._hsv = rgb2hsv(self._rgb)
        self._hsl = rgb2hsl(self._rgb)
        self._lch = new_lch
        return True

    @property
    def color_code(self) -> str:
        """カラーコードの現在値を取得する。

        Returns:
            str: カラーコードの現在値。
        """
        return self._color_code

    @property
    def rgb(self) -> RGB:
        """RGB の現在値を取得する。

        Returns:
            RGB: RGB の現在値。
        """
        return self._rgb

    @property
    def hsv(self) -> HSV:
        """HSV の現在値を取得する。

        Returns:
            HSV: HSV の現在値。
        """
        return self._hsv

    @property
    def hsl(self) -> HSL:
        """HSL の現在値を取得する。

        Returns:
            HSL: HSL の現在値。
        """
        return self._hsl

    @property
    def lch(self) -> Lch:
        """LCh の現在値を取得する。

        Returns:
            Lch: LCh の現在値。
        """
        return self._lch
